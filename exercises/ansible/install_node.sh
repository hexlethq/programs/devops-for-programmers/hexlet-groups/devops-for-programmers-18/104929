#!/usr/bin/env bash

curl -fsSL https://deb.nodesource.com/setup_15.x | sudo -E bash -
apt-get install -y nodejs
npm install -g npm